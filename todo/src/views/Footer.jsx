import React, { useState } from "react";
import styled from "styled-components";

// import styled from "styled-components";

const Footer = ({list,setter}) => {

    const deleteComp = () =>{
            const dupList = list.filter((item) => !item.marked);
            setter(dupList);
        }

    const displayActive=()=>{
        let newList=list.map((item)=>{
            if(item.marked){
                return {...item,display:false};
            }
            else{
                return {...item,display:true};
            }
        });
        setter(newList);
        console.log(list);
    }
    const displayNonActive=()=>{
        let newList=list.map((item)=>{
            if(item.marked){
                return {...item,display:true};
            }
            else{
                return {...item,display:false};
            }
        });
        setter(newList);
        console.log(list);
    }
    const displayAll=()=>{
        let newList=[];
        for(let item of list) {
                newList.push({...item,display:true});
        }
        setter(newList);
        console.log(list);
    }

    return (list.length>0) ?  <Box>
        <p>{list.filter((x)=>!x.marked).length} items left</p>
        <div className="buttonsWrapper">
            <button onClick={displayAll}>All</button>
            <button onClick={displayActive}>Active</button>
            <button onClick={displayNonActive}>Completed</button>
        </div>
        {
        list.filter((x)=>x.marked).length>0 ? 
            <button className="deleteCompBTN" onClick={deleteComp}>Delete Completed</button>
            :
            null
}
    </Box> 
    :
    null
}

const Box=styled.div`
    display:flex;
    background-color:white;
    display:flex;
    justify-content:space-between;
    font-size:10px;
    padding-top:1rem;
    .buttonsWrapper{

    }
    .buttonsWrapper button{
        background-color:white;
        border:1px solid white;
        font-size:10px;
        padding:0 1rem ;
    }
    .buttonsWrapper button:hover{
        border:1px solid Cyan;
    }
    button:active{
        border:1px solid red;
    }
    .deleteCompBTN{
        background-color:white;
        border:1px solid white; 
        font-size:10px;

    }
    .deleteCompBTN:hover{
        border-bottom: 1px solid black;
    }
`;

export default Footer;