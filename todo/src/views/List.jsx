import React, { useState } from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircle} from '@fortawesome/free-regular-svg-icons'
import { faCheckCircle} from '@fortawesome/free-regular-svg-icons'
import { faTimesCircle} from '@fortawesome/free-regular-svg-icons'

// const UnCompletedItem = style.div``;
const CompletedItem = styled.span`
text-decoration: line-through`;

const LiElement = styled.li`
    display:flex;
    border-bottom: solid lightgrey 1px;
    padding: 1rem;
    font-size: 2.4rem;
    font-weight: 300;
    span{
        transform: translateX(10px);
        flex:1;
        text-align: left;
        font-family:-apple-system,BlinkMacSystemFont,"Roboto","Oxygen", "Ubuntu","Cantarell","Fira Sans","Droid Sans","Helvetica Neue", sans-serif;
    }
    .x{
            color:red;
            visibility:hidden;
           
    }
    .green{
        color:green;
    }
    &:hover{
        .x{
        visibility:visible;}
    }
`;
const set_list_flag = (list, set_list, item_id) => {
    return () => {
        const dupList = [...list];
        for(let listItem of dupList){
            if(listItem.id === item_id){
                listItem.marked =  !listItem.marked
                break;
            }
        }
        set_list(dupList);
    }
}
const removeItem = (list, set_list, item_id) =>{
    return () => {
        const dupList = list.filter((item) => item.id !== item_id);
        set_list(dupList);
    }
}


const List = ({list, setter}) => {
    console.log(list)
   
    return (<ul>{list.map((item)=>
      item.display ? 
        item.marked ? <LiElement key={item.id}><FontAwesomeIcon icon={faCheckCircle} className="green" onClick={set_list_flag(list,setter,item.id)}/><CompletedItem>{item.value}</CompletedItem><FontAwesomeIcon className="x" icon={faTimesCircle} onClick={removeItem(list,setter,item.id)}/></LiElement> :
        <LiElement key={item.id}><FontAwesomeIcon icon={faCircle} onClick={set_list_flag(list,setter,item.id)}/><span>{item.value}</span><FontAwesomeIcon className="x" icon={faTimesCircle} onClick={removeItem(list,setter,item.id)}/></LiElement> 
        : null
            
)}</ul>)
}

export default List